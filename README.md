# RockCollector

Application to make managing a collection of rock samples easier and more convenient, even when there are multiple people collection samples for the same collection.

The application also has a sort of blogging system where visits to locations can be detailed and/or documented.

Each collector has a profile they can fill you with a profile photo and some information about themselves.

All other models, such as locations, minerals etc. can also have a profile, filled out with various information.

## Routes

- /
- collection/
- sample/
    - sample_code/
- location/
    - location_id/
        - location_visit_id/
- mineral/
    - mineral_id/
- dashboard/
    - inventory/
    - locations/
        - edit/location_id/
        - add/
    - location_visits/
        - edit/location_visit_id/
        - add/
    - collectors/
        - edit/collector_id/
        - add/
    - minerals/
        - edit/mineral_id/
        - add/
    - cases/
    - samples/
        - edit/sample_code/
        - add/


## Apps

### locations

App to model locations and allow to store information about them. Location models are also used by other apps to store and link location information, such as addresses etc.

#### Countries

##### Fields

- Country code
- Country name

#### Provinces

##### Fields

- Province name
- Province country

#### Cities

##### Fields

- City name
- City postal code
- City province

#### Locations

##### Fields

- Location name
- Location street
- Location street number
- Location city

### collectors

#### Collector

##### Fields

- User
- profile_photo
- birthdate
- bio

### locationvisits / blog

### rocksamples

### dashboard / inventory

App for managing the collection on the frontend, by select users (filtered by group). Also allows to keep an inventory of the collection, keeping track of what samples are located where.
