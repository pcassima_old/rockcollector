from django.db import models


class Country(models.Model):
    country_name = models.CharField(max_length=16)
    country_code = models.CharField(max_length=2)

    class Meta:
        verbose_name = 'country'
        verbose_name_plural = 'countries'

    def __str__(self) -> str:
        return str(self.country_name)

    def __repr__(self) -> str:
        return f"{self.country_code} - {self.country_name}"

    def get_short_name(self) -> str:
        return str(self.country_code)

    def get_long_name(self) -> str:
        return str(self)

    def get_full_name(self) -> str:
        return repr(self)


class Province(models.Model):
    province_name = models.CharField(max_length=32)
    province_country = models.ForeignKey(Country, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'province'
        verbose_name_plural = 'provinces'

    def __str__(self) -> str:
        return f"{self.province_name}, {self.province_country}"

    def __repr__(self) -> str:
        return f"{self.province_name}, {repr(self.province_country)}"

    def get_short_name(self) -> str:
        return str(self.province_name)

    def get_long_name(self) -> str:
        return f"{self.province_name}, {self.province_country}"

    def get_full_name(self) -> str:
        return repr(self)


class City(models.Model):
    city_name = models.CharField(max_length=32)
    city_postal_code = models.CharField(max_length=12)
    city_province = models.ForeignKey(Province, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'city'
        verbose_name_plural = 'cities'

    def __str__(self) -> str:
        return f"({self.city_postal_code}) {self.city_name}, {self.city_province}"

    def __repr__(self) -> str:
        return f"{self}, {repr(self.city_province)}"

    def get_short_name(self) -> str:
        return str(self.city_name)

    def get_long_name(self) -> str:
        return f"{self}, {self.city_province}, {self.city_province.province_country}"

    def get_full_name(self) -> str:
        return repr(self)


class Location(models.Model):
    location_name = models.CharField(max_length=32)
    location_street = models.CharField(max_length=128, null=True, default=None, blank=True)
    location_street_number = models.CharField(max_length=8, null=True, default=None, blank=True)
    location_city = models.ForeignKey(City, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'location'
        verbose_name_plural = 'locations'

    def __str__(self) -> str:
        return str(self.location_name)

    def __repr__(self) -> str:
        return f"{self.location_name}; {self.location_street_number} {self.location_street}, {repr(self.location_city)}"

    def get_short_name(self) -> str:
        return str(self)

    def get_long_name(self) -> str:
        return f"{self.location_name}, {self.location_city} - {self.location_city.city_province.province_country}"

    def get_full_name(self) -> str:
        return repr(self)
