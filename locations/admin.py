from django.contrib import admin
from .models import Country, Province, City, Location


class CountryAdmin(admin.ModelAdmin):
    list_display = ('country_code', 'country_name',)
    search_fields = ('country_name', 'country_code',)
    ordering = ['country_code']


class ProvinceAdmin(admin.ModelAdmin):
    list_display = ('province_name', 'province_country')
    search_fields = ('province_name', 'province_country')
    ordering = ['province_country', 'province_name']
    autocomplete_fields = ['province_country']


class CityAdmin(admin.ModelAdmin):
    list_display = ('city_name', 'city_postal_code', 'city_province')
    search_fields = ('city_postal_code', 'city_name')
    ordering = ['city_province', 'city_postal_code', 'city_name']
    autocomplete_fields = ['city_province']


def location_address(location):
    return (location.location_street if location.location_street else '') + ' ' + (
        location.location_street_number if location.location_street_number else '')


class LocationAdmin(admin.ModelAdmin):
    list_display = ('location_name', location_address, 'location_city')
    search_fields = ('location_name', 'location_city')
    ordering = ['location_city', 'location_name']
    autocomplete_fields = ['location_city']


admin.site.register(Country, CountryAdmin)
admin.site.register(Province, ProvinceAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Location, LocationAdmin)
